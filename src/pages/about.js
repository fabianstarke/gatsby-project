import React from "react"
import { Link } from "gatsby"
import Layout from "../components/layout"
import Head from "../components/head"

const AboutPage = () => {
  return (
    <Layout>
      <Head title="About" />
      <h1>About Me</h1>
      <p>This is my Bio</p>
      <p>This is just a new paragraph to my Bio</p>
      <p>
        Contact me <Link to="/contact">contact me</Link>
      </p>
    </Layout>
  )
}

export default AboutPage
