import React from 'react'
import Layout from "../components/layout"
import Head from "../components/head"

const ContactPage = () => {
  return (
    <Layout>
      <Head title="Contact" />
      <h1>My Contacts</h1>
      <p>FirstName</p>
      <p>LastName</p>
      <a href="https://twitter.com/Fabstarke" target="_blank">
        Twitter
      </a>
    </Layout>
  )
}

export default ContactPage
